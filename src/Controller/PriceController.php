<?php

namespace App\Controller;

use App\DTO\Request\PriceDTO;
use App\Services\PriceServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class PriceController extends AbstractController
{
    public function __construct(
        private readonly PriceServices $priceServices
    ) {
    }

    #[Route('/price', name: 'price', methods: ['POST'])]
    public function index(PriceDTO $DTO): JsonResponse
    {
        //        $decoded = json_decode($request->getContent(), true);
        //        $price = $decoded['price'];
        //        $startDate = $decoded['startDate'];
        //        $brithDayDate = $decoded['brithDayDate'];
        //        $payDate = $decoded['payDate'];

        $totalPrice = $this->priceServices->priceCalculate(
            $DTO->getPrice(),
            $DTO->getStartDate(),
            $DTO->getBrithDayDate(),
            $DTO->getPayDate()
        );

        return new JsonResponse(['totalPrice' => $totalPrice]);
    }
}
